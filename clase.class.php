<?php

    class Persona{
        private $nombre;
        private $apellido;
        private $edad;

        function __construct()
        {
            echo "Hola, soy un constructor";
            $this->nombre="";
            $this->apellido="";
            $this->edad=0;
        }

        function getNombre(){
            return ($this->nombre);
        }

        function getApellido(){
            return ($this->apellido);
        }

        function getEdad(){
            return ($this->edad);
        }

        function setNombre($nombre){
            $this->nombre=$nombre;
        }

        function setApellido($apellido){
            $this->apellido=$apellido;
        }

        function setEdad($edad){
            $this->edad=$edad;
        }


    }

?>