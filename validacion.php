<!DOCTYPE HTML>
<html>

<head>
</head>

<body>

    <?php
    $nombre = $email = $genero = $comentario = $website = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nombre = test_input($_POST["nombre"]);
        $email = test_input($_POST["email"]);
        $website = test_input($_POST["website"]);
        $comentario = test_input($_POST["comentario"]);
        $genero = test_input($_POST["genero"]);
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    ?>

    <h2>Ejemplo de validacion</h2>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Nombre: <input type="text" name="nombre">
        <br><br>
        E-mail: <input type="text" name="email">
        <br><br>
        Website: <input type="text" name="website">
        <br><br>
        Commentario: <textarea name="comentario" rows="5" cols="40"></textarea>
        <br><br>
        Genero:
        <input type="radio" name="genero" value="mujer">Mujer
        <input type="radio" name="genero" value="hombre">Hombre
        <input type="radio" name="genero" value="otro">Otro
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    echo "<h2>Datos:</h2>";
    echo $nombre;
    echo "<br>";
    echo $email;
    echo "<br>";
    echo $website;
    echo "<br>";
    echo $comentario;
    echo "<br>";
    echo $genero;
    ?>

</body>

</html>